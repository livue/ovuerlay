import { library, config } from '@fortawesome/fontawesome-svg-core';
import { faDiscord, faUbuntu } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { fas } from "@fortawesome/free-solid-svg-icons";

config.autoAddCss = false;
library.add(fas, faDiscord, faUbuntu)

export default defineNuxtPlugin((nuxtApp) => {
    nuxtApp.vueApp.component('font-awesome-icon', FontAwesomeIcon, {})
})


